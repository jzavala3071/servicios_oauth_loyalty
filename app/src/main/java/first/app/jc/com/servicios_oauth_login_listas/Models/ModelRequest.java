package first.app.jc.com.servicios_oauth_login_listas.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zac on 24/03/2018.
 */

public class ModelRequest {

//country_code=MX
// &username=pruebas-pruebas-ia%40outlook.com
// &password=Pruebas01
// &grant_type=password
// &client_id=CinepolisAndroidMobile
// &client_secret=129ed5aae0a74176b54dd597a47bfe63

    @SerializedName("country_code")
    private String countryCode;

    private String username;
    private String password;

    @SerializedName("grant_type")
    private String grantType;

    @SerializedName("client_id")
    private String clientID;

    @SerializedName("client_secret")
    private String clientSecret;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}
