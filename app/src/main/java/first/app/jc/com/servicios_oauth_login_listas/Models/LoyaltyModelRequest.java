package first.app.jc.com.servicios_oauth_login_listas.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jzavala on 3/26/2018.
 */

public class LoyaltyModelRequest {
    @SerializedName("card_number")
    private String cardNumber;

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("transaction_include")
    private boolean transacionInclude;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isTransacionInclude() {
        return transacionInclude;
    }

    public void setTransacionInclude(boolean transacionInclude) {
        this.transacionInclude = transacionInclude;
    }
}

//pruebas-pruebas-ia@outlook.com