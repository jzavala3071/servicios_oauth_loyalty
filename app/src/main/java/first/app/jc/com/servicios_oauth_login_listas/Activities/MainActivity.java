package first.app.jc.com.servicios_oauth_login_listas.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import first.app.jc.com.servicios_oauth_login_listas.Interfaces.OauthServiceInterface;
import first.app.jc.com.servicios_oauth_login_listas.Models.ModelRequest;
import first.app.jc.com.servicios_oauth_login_listas.Models.ModelResponse;
import first.app.jc.com.servicios_oauth_login_listas.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private EditText user_email;
    private EditText password;
    private Button button_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user_email = findViewById(R.id.edit_text_email);
        password = findViewById(R.id.edit_text_pass);
        button_login = findViewById(R.id.btn_login);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    CallToServiceRetrofit(user_email.getText().toString(), password.getText().toString());
                }
            }
        });


    }

    private void CallToServiceRetrofit(final String user_email, String password){
        String ENDPOINT = getString(R.string.host_api_oauth);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ModelRequest modelRequest = new ModelRequest();
        modelRequest.setClientID("CinepolisAndroidMobile");
        modelRequest.setClientSecret("129ed5aae0a74176b54dd597a47bfe63");
        modelRequest.setCountryCode("MX");
        modelRequest.setGrantType("password");
        modelRequest.setPassword(password);
        modelRequest.setUsername(user_email);

        OauthServiceInterface oauthServiceInterface = retrofit.create(OauthServiceInterface.class);

        oauthServiceInterface.getAccessOauth("MX",
                                        user_email,
                                        password,
                                        "password",
                                        "CinepolisAndroidMobile",
                                        "129ed5aae0a74176b54dd597a47bfe63")
                .enqueue(new Callback<ModelResponse>() {

            @Override
            public void onResponse(Call<ModelResponse> call, Response<ModelResponse> response) {
                if(response.code() == 400){
                    Toast.makeText(MainActivity.this, "Error en las credenciales",Toast.LENGTH_SHORT).show();
                    clearFields();
                }else{
                    //Log.e("TAG", "sucess");
                    Intent intent = new Intent(MainActivity.this,LoyaltyActivity.class);
                    intent.putExtra("accessToken",response.body().getAccessToken());
                    intent.putExtra("tokenType", response.body().getTokenType());
                    startActivity(intent);
                }

            }

            @Override
            public void onFailure(Call<ModelResponse> call, Throwable t) {
                Log.e("TAG2", "onFailure");
            }
        });

    }

    private boolean validate(){
        boolean flag = true;

        if(user_email.getText().toString().isEmpty()){
            user_email.setError(getString(R.string.label_email_empty));
            flag = false;
        }

        if(password.getText().toString().isEmpty()){
            password.setError(getString(R.string.label_password_empty));
            flag = false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(user_email.getText().toString()).matches()){
            user_email.setError(getString(R.string.label_email_wrong));
            flag = false;
        }
        return flag;
    }

    void clearFields(){
        user_email.setText("");
        password.setText("");
    }
}
