package first.app.jc.com.servicios_oauth_login_listas.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import first.app.jc.com.servicios_oauth_login_listas.Adapters.AdapterRecyclerTransactions;
import first.app.jc.com.servicios_oauth_login_listas.Interfaces.LoyaltyServiceInterface;
import first.app.jc.com.servicios_oauth_login_listas.Models.LoyaltyModelRequest;
import first.app.jc.com.servicios_oauth_login_listas.Models.LoyaltyModelResponse;
import first.app.jc.com.servicios_oauth_login_listas.Models.ModelRecyclerTransactions;
import first.app.jc.com.servicios_oauth_login_listas.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoyaltyActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterRecyclerTransactions adapterRecyclerTransactions;
    private List<ModelRecyclerTransactions> list;
    private TextView tv_name;
    private TextView tv_email;
    private TextView tv_level;
    private EditText et_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loyalty);

        tv_name = findViewById(R.id.tv_loyalty_name);
        tv_email = findViewById(R.id.tv_loyalty_email);
        tv_level = findViewById(R.id.tv_loyalty_level);
        et_card = findViewById(R.id.et_card);
        recyclerView = findViewById(R.id.rv_loyalty_transactions);

        String card = "1303030317482433";

        callRetrofitService(getIntent().getExtras().getString("accessToken"),getIntent().getExtras().getString("tokenType"), card);

        et_card.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence,  int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                //Log.e("tag",String.valueOf(count));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 16){
                   //card = et_card.getText().toString();
                   callRetrofitService(getIntent().getExtras().getString("accessToken"),getIntent().getExtras().getString("tokenType"), et_card.getText().toString());

                }
            }
        });


    }

    private void callRetrofitService(String accessToken, String tokenType, String card){
        String ENDPOINT = getString(R.string.host_api_loyalty);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        LoyaltyModelRequest modelRequest = new LoyaltyModelRequest();
        modelRequest.setCardNumber(card); //1303030317482433//1303030744601039
        modelRequest.setCountryCode("MX");
        modelRequest.setTransacionInclude(true);
        //v1/members/loyalty

        LoyaltyServiceInterface loyaltyServiceInterface = retrofit.create(LoyaltyServiceInterface.class);
        loyaltyServiceInterface.getMembersLoyalty(tokenType+" "+accessToken,modelRequest).enqueue(new Callback<LoyaltyModelResponse>() {
            @Override
            public void onResponse(Call<LoyaltyModelResponse> call, Response<LoyaltyModelResponse> response) {
                if(response.code() == 400){
                    Toast.makeText(LoyaltyActivity.this, "Error de CARD",Toast.LENGTH_SHORT).show();
                }else{
                    tv_name.setText("Nombre: "+response.body().getName());
                    tv_email.setText("Email: "+response.body().getEmail());
                    tv_level.setText("Nivel: "+response.body().getLevel().getNextLevel());

                    adapterRecyclerTransactions = new AdapterRecyclerTransactions(response.body().getTransactions(), LoyaltyActivity.this);
                    recyclerView.setAdapter(adapterRecyclerTransactions);
                    recyclerView.setLayoutManager(new LinearLayoutManager(LoyaltyActivity.this,LinearLayoutManager.VERTICAL, false));
                    recyclerView.hasFixedSize();
                }
            }

            @Override
            public void onFailure(Call<LoyaltyModelResponse> call, Throwable t) {
                Log.e("TAG", "onFailure");
            }
        });

    }
}
