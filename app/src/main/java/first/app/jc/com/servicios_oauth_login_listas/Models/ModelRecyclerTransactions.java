package first.app.jc.com.servicios_oauth_login_listas.Models;

/**
 * Created by zac on 24/03/2018.
 */

public class ModelRecyclerTransactions {

    private String cinema;
    private String message;
    private String date;
    private Integer points;

    public ModelRecyclerTransactions(String cinema, String message, String date, Integer points) {
        this.cinema = cinema;
        this.message = message;
        this.date = date;
        this.points = points;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
