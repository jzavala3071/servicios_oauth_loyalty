package first.app.jc.com.servicios_oauth_login_listas.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jzavala on 3/26/2018.
 */

public class LoyaltyModelResponse {

    private String name;
    private String email;
    private String message;
    private List<Balance_list> balance_list;
    private Level level;
    private List<ModelRecyclerTransactions> transactions;
    //private List<Transactions> transactions;


    @SerializedName("status_code")
    private int statusCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<Balance_list> getBalance_list() {
        return balance_list;
    }

    public void setBalance_list(List<Balance_list> balance_list) {
        this.balance_list = balance_list;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public List<ModelRecyclerTransactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<ModelRecyclerTransactions> transactions) {
        this.transactions = transactions;
    }

    public class Balance_list{
        private float balance;
        private String key;
        private String name;
        private String message;

        public float getBalance() {
            return balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class Level{

        @SerializedName("next_level")
        String nextLevel;

        @SerializedName("advance_percent")
        float advancePercent;

        @SerializedName("key")
        String key;

        @SerializedName("name")
        String name;

        @SerializedName("message")
        String message;

        public String getNextLevel() {
            return nextLevel;
        }

        public void setNextLevel(String nextLevel) {
            this.nextLevel = nextLevel;
        }

        public float getAdvancePercent() {
            return advancePercent;
        }

        public void setAdvancePercent(float advancePercent) {
            this.advancePercent = advancePercent;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
/*

    public class Transactions{
        private String cinema;
        private String message;
        private String date;
        private int points;

        public String getCinema() {
            return cinema;
        }

        public void setCinema(String cinema) {
            this.cinema = cinema;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }
    }
*/

}
