package first.app.jc.com.servicios_oauth_login_listas.Interfaces;

import first.app.jc.com.servicios_oauth_login_listas.Models.ModelResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by zac on 24/03/2018.
 */

public interface OauthServiceInterface {

    @Headers({"api_key:cinepolis_test_android",
                "Content-Type:application/x-www-form-urlencoded"})
    @FormUrlEncoded
    @POST("v2/oauth/token")

    //Call<ModelResponse> getAccessOauth (@Body ModelRequest bodyAccessOauth);
    Call<ModelResponse> getAccessOauth(
            @Field("country_code") String countryCode,
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String grant_type,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret
            );

}
