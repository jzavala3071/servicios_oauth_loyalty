package first.app.jc.com.servicios_oauth_login_listas.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import first.app.jc.com.servicios_oauth_login_listas.Models.ModelRecyclerTransactions;
import first.app.jc.com.servicios_oauth_login_listas.R;

/**
 * Created by zac on 24/03/2018.
 */

public class AdapterRecyclerTransactions extends RecyclerView.Adapter<AdapterRecyclerTransactions.ItemViewHolder> {

    private List<ModelRecyclerTransactions> mList = new ArrayList();
    private Context context;

    public AdapterRecyclerTransactions(List<ModelRecyclerTransactions> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loyalty_items,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        //personModel model = mList.get(position);
        ModelRecyclerTransactions model = mList.get(position);

        holder.cinema.setText(model.getCinema());
        holder.message.setText(model.getMessage());

        holder.date.setText(getDate(model));

        holder.points.setText(String.valueOf(model.getPoints()));

    }

    String getDate(ModelRecyclerTransactions model){
        String fecha = model.getDate();
        SimpleDateFormat parseador = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd H:mm a", Locale.US);

        try {

            Date date_parse = parseador.parse(fecha);
            return formateador.format(date_parse);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView cinema, message,date,points;

        public ItemViewHolder(View itemView) {
            super(itemView);

            cinema = itemView.findViewById(R.id.tv_transactions_cinema);
            message = itemView.findViewById(R.id.tv_transactions_message);
            date = itemView.findViewById(R.id.tv_transactions_date);
            points = itemView.findViewById(R.id.tv_transactions_points);

        }
    }
}
