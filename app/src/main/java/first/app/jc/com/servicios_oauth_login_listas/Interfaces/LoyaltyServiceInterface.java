package first.app.jc.com.servicios_oauth_login_listas.Interfaces;

import first.app.jc.com.servicios_oauth_login_listas.Models.LoyaltyModelRequest;
import first.app.jc.com.servicios_oauth_login_listas.Models.LoyaltyModelResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by jzavala on 3/26/2018.
 */

public interface LoyaltyServiceInterface {

    @Headers({"api_key:cinepolis_test_android"
    })

    @POST("v1/members/loyalty/")

    Call<LoyaltyModelResponse> getMembersLoyalty (@Header("Authorization") String authorization, @Body LoyaltyModelRequest bodyMembersLoyalty);
}
